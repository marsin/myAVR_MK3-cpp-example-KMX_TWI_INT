/******************************************************************************
 * myAVR-MK3_KMX-TWI-INT
 * =====================
 *
 *   -  myAVR-MK3 Key Matrix (KMX) on TWI (I2C) bus with Interrrupt
 *
 * Copyright (c) 2017 Martin Singer <martin.singer@web.de>
 *
 * This file is part of MyAVR-MK3_KMX-TWI-INT
 *
 *    MyAVR_MK3_KMX-TWI-INT is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MyAVR_MK3_KMX-TWI-INT is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with MyAVR_MK3_KMX-TWI-INT.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/


/// Translate key events to scancode events and buffers them.
///
/// @file scancode.cc
/// @author Martin Singer


#include "scancode.h"


Scancode::Scancode()
{
	Flag_EventAvailable = false;

	for (uint8_t i = 0; i < SCANCODE_FIFO_SIZE; ++i) {
		Event.Scancode[i] = 0x00;
	}
	Event.ReadPosition = 0;
	Event.WritePosition = 0;
	Event.BufferFull = false;
}


/** Create and buffer scancode
 *
 * @param condition event condition - COND_RELEASED, COND_PRESSED
 * @param side      1st or 2nd KMX  - SIDE_LEFT, SIDE_RIGHT
 * @param row       row             - 0..7
 * @param column    column          - 0..7
 *
 * Creates scancode from the different parameters
 * and bufferes it in the Event_Fifo struct.
 *
 * The write position points always to the next buffer slot.
 * After a write action the write position becomes incremented.
 * Is pos_write larger or equal to SIZE_FIFO it becomes set to 0.
 *
 * TODO: Is the pos_write equal to pos_read a flag becomes set
 * to prevent from writing into already not read event slots.
 * The flag becomes deleted by the read function.
 */
void
Scancode::pushKeyEvent(uint8_t condition,
                       uint8_t side,
                       uint8_t row,
                       uint8_t column)
{
	uint8_t scancode = (condition<<7) | (side<<6) | (row<<3) | (column<<0);

	if (Event.BufferFull == false) {
		Event.Scancode[Event.WritePosition] = scancode;
		Flag_EventAvailable = true;

		++Event.WritePosition;
		if (Event.WritePosition >= SCANCODE_FIFO_SIZE) {
			Event.WritePosition = 0;
		}
		if (Event.WritePosition == Event.ReadPosition) {
			Event.BufferFull = true;
		}
	}
}


/** Get next scancode from Key Event Buffer.
 * Returns the next scancode from the Key Event Buffer.
 *
 * If the buffers read and write position is equivalent,
 * means there are no new events in the buffer.
 * If this condition happens, the Flag_Event becomes set to FLAG_NULL
 * which indicates this function sould not be called.
 * If it becomes called anyway, the function returns the default value 0x00
 * (which is a valid scancode).
 * There is no possiblity to signal the returned value is invalid!
 *
 * Is the Scancode read from the Buffer to a local varable,
 * the read position becomes incremented.
 * Is pos_read larger than SIZE_FIFO, pos_read is set to 0 (ring buffer).
 *
 * Is pos_read equivalent to pos_write, all actual events are fetched.
 * The Flag_Events becomes set to FLAG_NULL,
 * which indicates, there are no more Key Events.
 */
uint8_t
Scancode::pullKeyScancode(void)
{
	uint8_t scancode = 0x00;

	scancode = Event.Scancode[Event.ReadPosition];
	Event.BufferFull = false;

	++Event.ReadPosition;
	if (Event.ReadPosition >= SCANCODE_FIFO_SIZE) {
		Event.ReadPosition = 0;
	}
	if (Event.ReadPosition == Event.WritePosition) {
		Flag_EventAvailable = false;
	}

	return scancode;
}


bool
Scancode::isEventAvailable(void)
{
	return Flag_EventAvailable;
}
