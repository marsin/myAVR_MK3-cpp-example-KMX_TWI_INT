/******************************************************************************
 * myAVR-MK3_KMX-TWI-INT
 * =====================
 *
 *   -  myAVR-MK3 Key Matrix (KMX) on TWI (I2C) bus with Interrrupt
 *
 * Copyright (c) 2017 Martin Singer <martin.singer@web.de>
 *
 * This file is part of MyAVR-MK3_KMX-TWI-INT
 *
 *    MyAVR_MK3_KMX-TWI-INT is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MyAVR_MK3_KMX-TWI-INT is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with MyAVR_MK3_KMX-TWI-INT.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/


/// Translate key events to scancode events and buffer them.
///
/// @file scancode.h
/// @author Martin Singer


#ifndef SCANCODE_H
#define SCANCODE_H

#include <avr/io.h>


#define SCANCODE_FIFO_SIZE 32
#define SCANCODE_SIDE_LEFT 0
#define SCANCODE_SIDE_RIGHT 1
#define SCANCODE_CONDITION_RELEASE 0
#define SCANCODE_CONDITION_PRESS 1


class Scancode {
	public:
		explicit Scancode();

		void pushKeyEvent(uint8_t, uint8_t, uint8_t, uint8_t);
		uint8_t pullKeyScancode(void);

		bool isEventAvailable(void);

	protected:
		bool Flag_EventAvailable;

		struct {
			uint8_t Scancode[SCANCODE_FIFO_SIZE];
			uint8_t ReadPosition;
			uint8_t WritePosition;
			bool BufferFull;
		} Event;
};


#endif // SCANCODE_H

