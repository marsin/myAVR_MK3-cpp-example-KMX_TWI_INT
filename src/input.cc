/******************************************************************************
 * myAVR-MK3_KMX-TWI-INT
 * =====================
 *
 *   -  myAVR-MK3 Key Matrix (KMX) on TWI (I2C) bus with Interrrupt
 *
 * Copyright (c) 2017 Martin Singer <martin.singer@web.de>
 *
 * This file is part of MyAVR-MK3_KMX-TWI-INT
 *
 *    MyAVR_MK3_KMX-TWI-INT is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MyAVR_MK3_KMX-TWI-INT is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with MyAVR_MK3_KMX-TWI-INT.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/


/// myAVR-MK3 Key Matrix (KMX) on TWI (I2C) bus with Interrrupt.
///
/// @file input.cc
/// @author Martin Singer


#include "input.h"
#include <util/atomic.h> // for ATOMIC_BLOCK(ATOMIC_RESTORESTATE)


#define TWI_ADDR_IC0 0x70 ///< 0111000x (right reader IC)
#define TWI_ADDR_IC1 0x72 ///< 0111001x (right writer IC)


Input::Input(Scancode* sc)
:
	scancodes(sc),
//	key_matrix_left(TWI_ADDR_IC3,
//	                TWI_ADDR_IC2,
//	                &debounce_timer,
//	                &scancodes,
//	                SCANCODE_SIDE_LEFT),
	key_matrix_right(TWI_ADDR_IC1,
	                 TWI_ADDR_IC0,
	                 &debounce_timer,
	                 scancodes,
	                 SCANCODE_SIDE_RIGHT)
{}


void
Input::call(void)
{
	checkKeyEvent();
	checkDebounceTimer();
}


bool
Input::isReadyForSleep(void)
{
	if (key_event.isNewKeyEventAvailable() == false &&
	    debounce_timer.isTimeUp() == false)
	{
		return true;
	}
	return false;
}


void
Input::checkKeyEvent(void)
{
	if (key_event.isNewKeyEventAvailable() == true) {
		ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
			key_matrix_right.pollForStage();
//			key_matrix_left.pollForStage();
			key_event.clearNewKeyEventAvailable();
		}
	}
}


void
Input::checkDebounceTimer(void)
{
	if (debounce_timer.isTimeUp() == true) {
		ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
			key_matrix_right.pollForState();
//			key_matrix_left.pollForState();
			debounce_timer.clearTimeUp();
		}
	}
}

