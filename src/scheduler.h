/******************************************************************************
 * myAVR-MK3_KMX-TWI-INT
 * =====================
 *
 *   -  myAVR-MK3 Key Matrix (KMX) on TWI (I2C) bus with Interrrupt
 *
 * Copyright (c) 2017 Martin Singer <martin.singer@web.de>
 *
 * This file is part of MyAVR-MK3_KMX-TWI-INT
 *
 *    MyAVR_MK3_KMX-TWI-INT is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MyAVR_MK3_KMX-TWI-INT is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with MyAVR_MK3_KMX-TWI-INT.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/


/// Scheduler Modul.
///
/// @file scheduler.h
/// @author Martin Singer


#ifndef SCHEDULER_H
#define SCHEDULER_H


#include "scancode.h"
#include "input.h"
#include "output.h"


/** Scheduler Class.
 *
 * ### Tasks
 *
 * - Calls the input and the output modules of the system
 *   in a loop.
 * - Sets the IC to sleep mode if no input or output events are pending.
 * - It contains a key scancode buffer
 *   with acces for the input and the output modules.
 */
class Scheduler {
	public:
		explicit Scheduler();
		void run(void);

	protected:
		void sleep(void);

		Scancode scancodes;
		Input input;
		Output output;
};


#endif // SCHEDULER_H

