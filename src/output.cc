/******************************************************************************
 * myAVR-MK3_KMX-TWI-INT
 * =====================
 *
 *   -  myAVR-MK3 Key Matrix (KMX) on TWI (I2C) bus with Interrrupt
 *
 * Copyright (c) 2017 Martin Singer <martin.singer@web.de>
 *
 * This file is part of MyAVR-MK3_KMX-TWI-INT
 *
 *    MyAVR_MK3_KMX-TWI-INT is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MyAVR_MK3_KMX-TWI-INT is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with MyAVR_MK3_KMX-TWI-INT.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/


/// myAVR-MK3 Key Matrix (KMX) on TWI (I2C) bus with Interrrupt.
///
/// @file output.cc
/// @author Martin Singer


#include "output.h"
#include <util/atomic.h> // for ATOMIC_BLOCK(ATOMIC_RESTORESTATE)


Output::Output(Scancode* sc)
:
	scancodes(sc)
{
	lcd.clearDisplay(); //TEST
	lcd.turnLightOn(true); //TEST

	temp_scancode = 0x00; //TEST
	counter = 0; //TEST
}


void
Output::call(void)
{
	checkOutputEvent();
}


bool
Output::isReadyForSleep(void)
{
	if (output_event.isTimeUp() == false &&
	    scancodes->isEventAvailable() == false)
	{
		return true;
	}
	return false;
}


void
Output::checkOutputEvent(void)
{
	if (output_event.isTimeUp() == true) {
		if (scancodes->isEventAvailable() == true) {
			ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
				temp_scancode = scancodes->pullKeyScancode();
				doLocalOutput();
				doRemoteOutput();
			}
		}
		output_event.clearTimeUp();
	}
}


/*
void
Output::checkOutputEvent(void)
{
	if (scancodes->isEventAvailable() == true) {
		ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
			temp_scancode = pullKeyScancode();
			doLocalOutput();
		}
	}

	if (output_event.isTimeUp() == true) {
		ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
			doRemoteOutput();
			output_event.clearTimeUp();
		}
	}
}
*/


void
Output::doLocalOutput(void)
{
	lcd.setCursorPosition(0,0);        //TEST
	lcd.printBinNumber(temp_scancode); //TEST
}


void
Output::doRemoteOutput(void)
{
	//TEST
	lcd.setCursorPosition(14,0);       //TEST
	lcd.printBinNumber(temp_scancode); //TEST
	lcd.setCursorPosition(14,2);
	lcd.printDecNumber(counter);
	if (counter >= 255) {
		counter = 0;
	} else {
		++counter;
	}
}

