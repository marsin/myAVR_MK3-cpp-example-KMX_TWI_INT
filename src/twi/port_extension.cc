/******************************************************************************
 * TWI Driver
 * ==========
 *
 *   - TWI (I2C) port extension IC driver
 *
 * Copyright (c) 2017 Martin Singer <martin.singer@web.de>
 *
 * This file is part of TWI Driver
 *
 *    TWI Driver is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    TWI Driver is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with TWI Driver.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/


/// TWI (I2C) port extension IC driver.
///
/// @file port_extension.cc
/// @author Martin Singer


#include "port_extension.h"


using namespace TWI;


PortExtension::PortExtension(uint8_t addr) :
	Address(addr)
{}


//PortExtension::~PortExtension()
//{}


uint8_t
PortExtension::readPort(void)
{
	uint8_t data = 0x00;
	uint8_t ret = startTransfer(Address | TWI_READ);

	if (0 != ret) {
		stopTransfer();
	} else {
		data = readDataNak(); // read and stop twi transfer
	}

	return data;
}


uint8_t
PortExtension::writePort(uint8_t data)
{
	uint8_t ret = startTransfer(Address | TWI_WRITE);

	if (0 != ret) {
		stopTransfer();
	} else {
		ret = writeData(data);
		stopTransfer();
	}

	return ret;
}

