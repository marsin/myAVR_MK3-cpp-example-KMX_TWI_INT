/******************************************************************************
 * TWI Driver
 * ==========
 *
 *   - TWI (I2C) master driver
 *
 * Copyright (c) 2017 Martin Singer <martin.singer@web.de>
 *
 * This file is part of TWI Driver
 *
 *    TWI Driver is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    TWI Driver is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with TWI Driver.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/


/// TWI (I2C) master driver.
///
/// @file master.cc
/// @author Peter Fleury,
///         Martin Singer


#include <util/twi.h>
#include "master.h"


/** TWI (I2C) Clock in Hz. */
#ifndef F_SCL
#define F_SCL 100000ul
#endif

using namespace TWI;


/** Initialize the TWI master interface.
 *
 * ## The Registers
 * - TWI Status Register - TWSR
 *   - Bits 7..3 - TWS:  TWI Status         (r)
 *   - Bit  2    - reserved                 (r)
 *   - Bits 1..0 - TWSP: TWI Prescaler Bits (r/w)
 *
 *     TWPS1 | TWPS0 | Prescaler Value
 *     ----- | ----- | ---------------
 *     0     | 0     | 1
 *     0     | 1     | 4
 *     1     | 0     | 16
 *     1     | 1     | 64
 *
 * - TWI Bit Rate Register - TWBR
 *   - Bits 7..0 - TWI Bit Rate Register (r/w)
 *
 * ## Calculating the TWBR value
 *   F_SCL = (F_CPU / (16 + 2(TWBR) * (4 exp TWPS)))
 * - TWBR = Value of the TWI Bit Rate Register
 *          (TWBR should be 10 or higher if the TWI operates in Master mode)
 * - TWPS = Value of the prescaler bits in the bits in the TWI Status Register
 *          (the value of the bits, not the value or the prescaler)
 */
Master::Master()
{
	TWSR = 0x00;
	TWBR = ((F_CPU / F_SCL) - 16) / 2;
}


/** Issues a start condition and sends address and transfer direction.
 *
 * @param  addr address and transfer direction of TWI device
 * @retval 0    device accessible
 * @retval 1    failed to access device
 *
 *
 * ## Usage
 *
 * The parameter 'addr' gets constructed like __[A6|A5|A4|A3|A2|A1|A0|R/W]__.
 * - 'Ax'  are the address bits
 * - 'R/W' the read/write bit. (0 := write; 1 := read)
 * - Use the TWI_WRITE and TWI_READ macro
 *
 * ### Examples
 *
 *	ret = start(0x70 | TWI_WRITE); // Address 000 for an PFC8574A: [0|1|1|1|A2|A1|A0|R/W]
 *	ret = start(0x40 | TWI_WRITE); // Address 000 for an PFC8574:  [0|1|0|0|A2|A1|A0|R/W]
 *
 *
 * ## The Registers
 * - TWI Control Register - TWCR
 *   - Bit 7 - TWINT: TWI Interrupt Flag         (r/w)
 *   - Bit 6 - TWEA:  TWI Enable Acknowledge Bit (r/w)
 *   - Bit 5 - TWSTA: TWI START Condition Bit    (r/w)
 *   - Bit 4 - TWSTO: TWI STOP  Condition Bit    (r/w)
 *   - Bit 3 - TWWC:  TWI Write Collision Flag   (r)
 *   - Bit 2 - TWEN:  TWI Enable Bit             (r/w)
 *   - Bit 1 - reserved                          (r)
 *   - Bit 0 - TWIE:  TWI Interrupt Enable       (r/w)
 */
uint8_t
Master::startTransfer(uint8_t addr)
{
	uint8_t twst = 0x00;

	// send start condition
	TWCR = (1<<TWINT) | (1<<TWSTA) | (1<<TWEN);

	// wait until transmission completed
	while (! (TWCR & (1<<TWINT)));

	// check value of TWI Status Register. Mask prescaler bits.
	twst = TW_STATUS & 0xF8;
	if ((twst != TW_START) && (twst != TW_REP_START)) {
		return 1;
	}

	// send device address
	TWDR = addr;
	TWCR = (1<<TWINT) | (1<<TWEN);

	// wait until transmission is completed and ACK/NAK has been received
	while (! (TWCR & (1<<TWINT)));

	// check value of TWI Stauts Register. Mask prescaler bits.
	twst = TW_STATUS & 0xF8;
	if ((twst != TW_MT_SLA_ACK) && (twst != TW_MR_SLA_ACK)) {
		return 1;
	}

	return 0;
}


/** Issues a repeated start and sends address and transfer direction.
 *
 * @param  addr address and transfer direction of TWI device
 * @retval 0    device accessible
 * @retval 1    failed to access device
 * @see    start()
 */
uint8_t
Master::repeatedStartTransfer(uint8_t addr)
{
	return startTransfer(addr);
}


/** Issues a start condition and sends address and transfer direction.
 *
 * @param addr address and transfer direction of TWI device
 * @see   start()
 *
 * If device is busy, use ack polling to wait until device is ready.
 */
void
Master::startTransferWaitForAck(uint8_t addr)
{
	uint8_t twst = 0x00;

	while (1) {
		// send START condition
		TWCR = (1<<TWINT) | (1<<TWSTA) | (1<<TWEN);

		// wait until transmission completed
		while (! (TWCR & (1<<TWINT)));

		// check value of TWI Status Rgister. Mask prescaler bits
		twst = TW_STATUS & 0xF8;
		if ((twst != TW_START) && (twst != TW_REP_START)) {
			continue;
		}

		// send device address
		TWDR = addr;
		TWCR = (1<<TWINT) | (1<<TWEN);

		// wait until transmission copleted
		while (! (TWCR & (1<<TWINT)));

		// check value of TWI Status Register. Mask prescaler bits
		twst = TW_STATUS & 0xF8;
		if ((twst == TW_MT_SLA_NACK) || (twst == TW_MR_DATA_NACK)) {

			// device busy, send stop condition to terminate write operation
			TWCR = (1<<TWINT) | (1<<TWEN) | (1<<TWSTO);

			// wait until stop condition is executed and bus released
			while (TWCR & (1<<TWSTO));
			continue;
		}
		break;
	}
	return;
}


/** Terminates the data transfer and releases the TWI bus. */
void
Master::stopTransfer(void)
{
	 // send stop condition
	TWCR = (1<<TWINT) | (1<<TWEN) | (1<<TWSTO);

	// wait until stop condition is executed and bus released
	while (TWCR & (1<<TWSTO));

	return;
}


/** Send one byte to TWI device.
 *
 * @param  data byte to be transfered
 * @retval 0    write successful
 * @retval 1    write failed
 */
uint8_t
Master::writeData(uint8_t data)
{
	uint8_t twst = 0x00;

	// send data to the previously addressed device
	TWDR = data;
	TWCR = (1<<TWINT) | (1<<TWEN);

	// wait until transmission completed
	while (! (TWCR & (1<<TWINT)));

	// check value of TWI Status Register. Mask prescaler bits
	twst = TW_STATUS & 0xF8;
	if (twst != TW_MT_DATA_ACK) {
		return 1;
	}

	return 0;
}


/** Read one byte from the TWI device, request more data from device.
 *
 * @return byte read from the TWI device
 */
uint8_t
Master::readDataAck(void)
{
	TWCR = (1<<TWINT) | (1<<TWEN) | (1<<TWEA);
	while (! (TWCR & (1<<TWINT)));
	return TWDR;
}


/** Read one byte from the TWI device, read is followed by stop condition.
 *
 * @return byte read from the TWI device
 */
uint8_t
Master::readDataNak(void)
{
	TWCR = (1<<TWINT) | (1<<TWEN);
	while (! (TWCR & (1<<TWINT)));
	return TWDR;
}


/** Read one byte from the TWI device.
 *
 * @param  ack  1 := send ACK, request more data from device<br>
 *              0 := send NAK, read is followed by stop condition
 * @return byte read from the TWI device
 *
 * Implemented as macro, which calls either read_ack() or read_nak()
 */
uint8_t
Master::readData(uint8_t ack)
{
	return ack ? readDataAck() : readDataNak();
}

