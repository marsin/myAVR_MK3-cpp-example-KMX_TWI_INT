/******************************************************************************
 * TWI Driver
 * ==========
 *
 *   - TWI (I2C) port extension IC driver
 *
 * Copyright (c) 2017 Martin Singer <martin.singer@web.de>
 *
 * This file is part of TWI Driver
 *
 *    TWI Driver is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    TWI Driver is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with TWI Driver.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/


/// TWI (I2C) port extension IC driver.
///
/// @file port_extension.h
/// @author Martin Singer


#ifndef TWI_PORT_EXTENSION_H
#define TWI_PORT_EXTENSION_H

#include <avr/io.h>
#include "master.h"

namespace TWI {

class PortExtension : protected Master {
	public:
		explicit PortExtension(uint8_t);
//		virtual ~PortExtension();

		uint8_t readPort();
		uint8_t writePort(uint8_t);

	protected:
		uint8_t Address;
};

}


#endif // TWI_PORT_EXTENSION_H


