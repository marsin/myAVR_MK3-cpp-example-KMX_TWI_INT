/******************************************************************************
 * TWI Driver
 * ==========
 *
 *   - TWI (I2C) master driver
 *
 * Copyright (c) 2017 Martin Singer <martin.singer@web.de>
 *
 * This file is part of TWI Driver
 *
 *    TWI Driver is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    TWI Driver is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with TWI Driver.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/


/// TWI (I2C) master driver.
///
/// @file master.h
/// @author Peter Fleury,
///         Martin Singer


/**
 * ## About
 *
 * - This driver is tested with PCF8574AP (PIC16) IC.
 * - Bases on the __twimaster library__ from __Peter Fleury__
 *   <http://jump.to/fleury> pfleury@gmx.ch.
 *
 *
 * ## Example
 *
 *	uint8_t ret = 0x00;
 *	
 *	ret = twi_start(0x70 | TWI_WRITE);
 *	if (ret == 0) {
 *		ret = twi_write(0xFF);
 *		twi_stop();
 *		if (ret == 0) {
 *			; // ok
 *		} else {
 *			; // fail
 *		}
 *	} else {
 *		twi_stop();
 *	}
 */


#ifndef TWI_MASTER_H
#define TWI_MASTER_H

#include <avr/io.h>

/** Defines the data direction in twi_start() and twi_rep_start().
 *
 * @def TWI WRITE READ
 *
 * # Example
 *	startTransfer(0x03 + TWI_READ);
 */
#define TWI_WRITE 0x00
#define TWI_READ  0x01


namespace TWI {

class Master {
	public:
		explicit Master();

		uint8_t startTransfer(uint8_t);
		uint8_t repeatedStartTransfer(uint8_t);
		void startTransferWaitForAck(uint8_t);
		void stopTransfer(void);
		uint8_t writeData(uint8_t);
		uint8_t readDataAck(void);
		uint8_t readDataNak(void);
		uint8_t readData(uint8_t);
};

} // namespace TWI


#endif // MYAVR_MK3__EXAMPLE_TEST__TWI_MASTER_H

