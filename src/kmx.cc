/******************************************************************************
 * myAVR-MK3_KMX-TWI-INT
 * =====================
 *
 *   -  myAVR-MK3 Key Matrix (KMX) on TWI (I2C) bus with Interrrupt
 *
 * Copyright (c) 2017 Martin Singer <martin.singer@web.de>
 *
 * This file is part of MyAVR-MK3_KMX-TWI-INT
 *
 *    MyAVR_MK3_KMX-TWI-INT is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MyAVR_MK3_KMX-TWI-INT is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with MyAVR_MK3_KMX-TWI-INT.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/


/// myAVR-MK3 Key Matrix (KMX) on TWI (I2C) bus with Interrrupt.
///
/// @file kmx.cc
/// @author Martin Singer


#include <util/delay.h> //TEST
#include "kmx.h"
#include "interrupt.h"


KMX::KMX()
:
	TwiWriterIC(0x00),
	TwiReaderIC(0x00),
	KeyStateColumns{0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF},
	KeyStageColumns{0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF},
	debounce(nullptr),
	Scancodes(nullptr),
	Side(0)
{}


KMX::KMX(uint8_t twi_writer_ic_addr,
         uint8_t twi_reader_ic_addr,
         DebounceTimer* debounce_timer,
         Scancode* scancodes,
         uint8_t side)
:
	TwiWriterIC(twi_writer_ic_addr),
	TwiReaderIC(twi_reader_ic_addr),
	KeyStateColumns{0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF},
	KeyStageColumns{0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF},
	debounce(debounce_timer),
	Scancodes(scancodes),
	Side(side)
{
	TwiWriterIC.writePort(0x00); // set all PINs to LOW
	TwiReaderIC.writePort(0xFF); // set all PINs to HIGH
}


void
KMX::pollForStage(void)
{
	for (uint8_t col_num = 0; col_num < 8; ++col_num) {
		TwiWriterIC.writePort(~(1<<col_num));
		KeyStageColumns[col_num] = TwiReaderIC.readPort();

		if (KeyStageColumns[col_num] != KeyStateColumns[col_num]) {
			debounce->startTimer();
		}

		// TEST: Prints the Stage Register on the LCD
//		LCD.setCursorPosition(0, col_num);
//		LCD.printBinNumber(KeyStageColumns[col_num]);
	}

	// Clean up twi TWI ports
	TwiWriterIC.writePort(0x00); // reset all PINs to LOW
	TwiReaderIC.writePort(0xFF); // reset all PINs to HIGH
}


void
KMX::pollForState(void)
{
	debounce->stopTimer();

	// Poll for divergent column between the State and the Stage
	for (uint8_t col_num = 0; col_num < 8; ++col_num) {
		if (KeyStageColumns[col_num] != KeyStateColumns[col_num]) {
			TwiWriterIC.writePort(~(1<<col_num));
			if (KeyStageColumns[col_num] == TwiReaderIC.readPort()) {

				// Poll for divergent row in this column between the State and the Stage (it's a single bit)
				for (uint8_t row_num = 0; row_num < 8; ++row_num) {
					if ((KeyStageColumns[col_num] & (1<<row_num)) != (KeyStateColumns[col_num] & (1<<row_num))) {

						// Buffer event as scancode
						if (Scancodes != nullptr) {
							if ((KeyStageColumns[col_num] & (1<<row_num)) == 0x00) {
								Scancodes->pushKeyEvent(SCANCODE_CONDITION_PRESS, Side, col_num, row_num);
							} else {
								Scancodes->pushKeyEvent(SCANCODE_CONDITION_RELEASE, Side, col_num, row_num);
							}
						}
					}
				}
				KeyStateColumns[col_num] = KeyStageColumns[col_num];
			}
		}

		// TEST: Prints the State Register on the LCD
//		LCD.setCursorPosition(14, col_num);
//		LCD.printBinNumber(KeyStateColumns[col_num]);
	}

	// Clean up twi TWI ports
	TwiWriterIC.writePort(0x00); // reset all PINs to LOW
	TwiReaderIC.writePort(0xFF); // reset all PINs to HIGH
}

