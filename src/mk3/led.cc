/******************************************************************************
 * myAVR-MK3_cpp-driver
 * ====================
 *
 *   - C++ driver for the MyAVR MK3 development board hardware
 *
 * Copyright (c) 2017 Martin Singer <martin.singer@web.de>
 *
 * This file is part of MyAVR-MK3_cpp-driver.
 *
 *    MyAVR-MK3_cpp-driver is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MyAVR-MK3_cpp-driver is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with MyAVR-MK3_cpp-driver.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/


/// Driver for the LEDs on the MyAVR MK3 development board.
///
/// @file   led.cc
/// @author Martin Singer


#include <avr/io.h>
#include "board.h"
#include "led.h"

using namespace MK3;


Led::Led()
{
	MK3_LED_DDR  = 0xFF; // config port as output
	MK3_LED_PORT = 0x00; // init LEDs off
}


/*
Led::~Led()
{
	;
}
*/


void
Led::writePort(uint8_t byte)
{
	MK3_LED_PORT = byte;
	return;
}


volatile uint8_t*
Led::readPort(void)
{
	return &MK3_LED_PORT;
}

