/******************************************************************************
 * MyAVR-MK3
 * =========
 *
 *   - driver for the MyAVR MK3 development board hardware
 *
 * Copyright (c) 2017 Martin Singer <martin.singer@web.de>
 *
 * This file is part of MyAVR-MK3.
 *
 *    MyAVR-MK3 is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MyAVR-MK3 is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with MyAVR-MK3.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/


/// Board definitions.
///
/// @file   board.h
/// @author Martin Singer


#ifndef MK3_BOARD_H
#define MK3_BOARD_H

#include <avr/io.h>


/// Oscilator frequency in Hz.
#ifndef F_CPU
#define F_CPU 16000000ul
#endif


// Ports
#define MK3_LCD_CTRL_DDR  DDRA  ///< LCD control port - direction out (write).
#define MK3_LCD_CTRL_PORT PORTA ///< LCD control port - configured as output.

#define MK3_LCD_DATA_DDR  DDRC  ///< LCD data port - direction in (read/write).
#define MK3_LCD_DATA_PORT PORTC ///< LCD data port - configured as output.
#define MK3_LCD_DATA_PIN  PINC  ///< LCD data port - configured as input.

#define MK3_LED_DDR       DDRL  ///< LED port - direction out (write).
#define MK3_LED_PORT      PORTL ///< LED port - configured as output (high active).

#define MK3_SEG7_DDR      DDRB  ///< 7 segment port - direction out (write).
#define MK3_SEG7_PORT     PORTB ///< 7 segment port - configured as output (high active).

#define MK3_JOY_DDR       DDRK  ///< Joysitick - direction in (read).
#define MK3_JOY_PORT      PORTK ///< Joysitick - pull-up resistors.
#define MK3_JOY_PIN       PINK  ///< Joysitick - input (low active).

#define MK3_SW_DDR        DDRH  ///< Switches - direction in (read).
#define MK3_SW_PORT       PORTH ///< Switches - pull-up resistors.
#define MK3_SW_PIN        PINH  ///< Switches - input (low active).

//#define MK3_ADC_DDR       DDRF
//#define MK3_ADC_PORT      PORTF
//#define MK3_ADC_PIN       PINF


#endif // MK3_BOARD_H

