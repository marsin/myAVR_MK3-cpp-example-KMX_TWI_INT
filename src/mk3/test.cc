/******************************************************************************
 * myAVR-MK3_cpp-driver
 * ====================
 *
 *   - C++ driver for the MyAVR MK3 development board hardware
 *
 * Copyright (c) 2017 Martin Singer <martin.singer@web.de>
 *
 * This file is part of MyAVR-MK3_cpp-driver.
 *
 *    MyAVR-MK3_cpp-driver is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    MyAVR-MK3_cpp-driver is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with MyAVR-MK3_cpp-driver.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/


/// Driver for the MyAVR MK3 development board.
///
/// @file main.cc
/// @author Martin Singer


#ifndef F_CPU
#define F_CPU 16000000ul
#endif

#include <avr/io.h>
#include <util/delay.h>
#include "led.h"
#include "seven_segment.h"
#include "switches.h"
#include "joystick.h"
#include "lcd.h"
#include "lcd_fonts.h"


void led_test(MK3::Led);
void seven_segment_test(MK3::SevenSegment);
void switches_test(MK3::Switches, MK3::Led);
void joystick_test(MK3::Joystick, MK3::Led);
void lcd_test(MK3::Lcd);


int main(void)
{
	MK3::Led LEDs;
	MK3::SevenSegment Segment;
	MK3::Switches Switches;
	MK3::Joystick Joystick;
	MK3::Lcd LCD;


#ifdef TEST_SWITCHES
	switches_test(Switches, LEDs);
#endif


#ifdef TEST_JOYSTICK
	joystick_test(Joystick, LEDs);
#endif


#ifdef TEST_LCD
	lcd_test(LCD);
#endif


	while (1) {
#ifdef TEST_LEDS
		led_test(LEDs);
#endif


#ifdef TEST_SEVEN_SEGMENT
		seven_segment_test(Segment);
#endif
	}

	return 0;
}


#ifdef TEST_LEDS
void led_test(MK3::Led Leds)
{
	for (unsigned int i=0; i <= 0xFF; ++i) {
		Leds.writePort(i);
		_delay_ms(250);
	}
	return;
}
#endif // TEST_LEDS


#ifdef TEST_SEVEN_SEGMENT
void seven_segment_test(MK3::SevenSegment Segment)
{
	for (unsigned int i=0; i <= 0xFF; ++i) {
		Segment.writePort(i);
//		Segment.printDigit(i % 0x0F);
		_delay_ms(250);
	}
	return;
}
#endif // TEST_SEVEN_SEGMENT


#ifdef TEST_SWITCHES
void switches_test(MK3::Switches Switches, MK3::Led LEDs)
{
	while (1) {
		LEDs.writePort(*Switches.readPin());
	}
	return;
}
#endif // TEST_SWITCHES


#ifdef TEST_JOYSTICK
void joystick_test(MK3::Joystick Joystick, MK3::Led LEDs)
{
	while (1) {
		LEDs.writePort(*Joystick.readPin());
	}
	return;
}
#endif // TEST_JOYSTICK


#ifdef TEST_LCD
void lcd_test(MK3::Lcd LCD)
{
	volatile const uint8_t *pt_font = Font5x7;

	while (1) {
		LCD.turnLightOn(true);
		_delay_ms(2500);

		LCD.clearDisplay();
		_delay_ms(2500);

		LCD.printCharacter('H', pt_font);
		LCD.printCharacter('e', pt_font);
		LCD.printCharacter('l', pt_font);
		LCD.printCharacter('l', pt_font);
		LCD.printCharacter('o', pt_font);
		LCD.printCharacter(' ', pt_font);
		LCD.printCharacter('W', pt_font);
		LCD.printCharacter('o', pt_font);
		LCD.printCharacter('r', pt_font);
		LCD.printCharacter('l', pt_font);
		LCD.printCharacter('d', pt_font);
		LCD.printCharacter('!', pt_font);
		_delay_ms(2500);

		LCD.shiftToLine(37);
		_delay_ms(2500);
		LCD.shiftToLine(0);
		_delay_ms(2500);

		LCD.setPointerPosition(21, 35);
		LCD.printCharacter('A', pt_font);
		LCD.printCharacter('B', pt_font);
		LCD.printCharacter('C', pt_font);
		_delay_ms(2500);

		LCD.drawPixel(45, 49);
		_delay_ms(2500);

		LCD.printCharacter('X', pt_font);
		LCD.printCharacter('Y', pt_font);
		LCD.printCharacter('Z', pt_font);
		_delay_ms(2500);

		LCD.inverseDisplayColors(true);
		_delay_ms(2500);
		LCD.inverseDisplayColors(false);
		_delay_ms(2500);

		LCD.turnDisplayOn(false);
		LCD.printCharacter('!', pt_font);
		LCD.printCharacter('@', pt_font);
		LCD.printCharacter('#', pt_font);
		_delay_ms(2500);
		LCD.turnDisplayOn(true);
		_delay_ms(2500);

		LCD.printCharacter('1', pt_font);
		LCD.printCharacter('2', pt_font);
		LCD.printCharacter('3', pt_font);
		_delay_ms(2500);

		LCD.turnLightOn(false);
		LCD.printCharacter('&', pt_font);
		LCD.printCharacter('*', pt_font);
		LCD.printCharacter('(', pt_font);
		_delay_ms(2500);
	}
	return;
}
#endif // TEST_LCD

